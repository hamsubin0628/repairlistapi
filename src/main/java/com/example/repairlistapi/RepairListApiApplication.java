package com.example.repairlistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepairListApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepairListApiApplication.class, args);
    }

}
