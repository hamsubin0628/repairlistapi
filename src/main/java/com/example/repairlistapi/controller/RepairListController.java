package com.example.repairlistapi.controller;

import com.example.repairlistapi.entity.RepairList;
import com.example.repairlistapi.model.RepairListItem;
import com.example.repairlistapi.model.RepairListRequest;
import com.example.repairlistapi.model.RepairListResponse;
import com.example.repairlistapi.service.RepairListService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/repair")
public class RepairListController {
    private final RepairListService repairListService;

    @PostMapping("/list")
    public String setRepairList(@RequestBody RepairListRequest request){
        repairListService.setRepairListRepository(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<RepairListItem> getRepairLists (){
        return repairListService.getRepairLists();
    }

    @GetMapping("/detail/{id}")
    public RepairListResponse getRepairList(@PathVariable long id){
        return repairListService.getRepairList(id);
    }
}
