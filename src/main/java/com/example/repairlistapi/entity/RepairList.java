package com.example.repairlistapi.entity;

import com.example.repairlistapi.enums.AccidentContent;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class RepairList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate accidentDate;

    @Column(nullable = false, length = 50)
    private String accidentPlace;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private AccidentContent accidentContent;

    @Column
    private Boolean mistake;

    @Column(nullable = false, length = 20)
    private String repairShop;

    @Column(nullable = false, length = 30)
    private String maintenanceDetails;

    @Column(nullable = false)
    private Double totalPayment;

    @Column(nullable = false)
    private Boolean insuranceApplication;

    @Column
    private Double refundPrice;

    @Column
    private String attackerName;

    @Column
    private String phoneNumber;

    @Column
    private String etcMemo;
}
