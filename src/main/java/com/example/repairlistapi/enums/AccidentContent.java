package com.example.repairlistapi.enums;

import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccidentContent {
    CAR_CAR("차대차 사고"),
    CAR_HUMAN("차 대 사람 사고"),
    SINGLE("단독사고");

    public final String accidentContentEnum;
}
