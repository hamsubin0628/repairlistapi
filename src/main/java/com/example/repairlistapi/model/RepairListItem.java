package com.example.repairlistapi.model;

import com.example.repairlistapi.enums.AccidentContent;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RepairListItem {
    private Long id;
    private LocalDate accidentDate;
    private String accidentPlace;
    private String accidentContent;
    private Boolean mistake;
    private String repairShop;
    private String maintenanceDetails;
    private Double totalPayment;
    private Boolean insuranceApplication;
    private Double refundPrice;
    private String attackerName;
    private String phoneNumber;
    private String etcMemo;
}
