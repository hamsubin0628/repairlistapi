package com.example.repairlistapi.repository;

import com.example.repairlistapi.entity.RepairList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepairListRepository extends JpaRepository<RepairList, Long> {
}
