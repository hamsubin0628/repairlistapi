package com.example.repairlistapi.service;

import com.example.repairlistapi.entity.RepairList;
import com.example.repairlistapi.model.RepairListItem;
import com.example.repairlistapi.model.RepairListRequest;
import com.example.repairlistapi.model.RepairListResponse;
import com.example.repairlistapi.repository.RepairListRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.dialect.unique.CreateTableUniqueDelegate;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RepairListService {
    private final RepairListRepository repairListRepository;

    public void setRepairListRepository(RepairListRequest repairListRequest){
        RepairList addData = new RepairList();
        addData.setAccidentDate(repairListRequest.getAccidentDate());
        addData.setAccidentPlace(repairListRequest.getAccidentPlace());
        addData.setAccidentContent(repairListRequest.getAccidentContent());
        addData.setMistake(repairListRequest.getMistake());
        addData.setRepairShop(repairListRequest.getRepairShop());
        addData.setMaintenanceDetails(repairListRequest.getMaintenanceDetails());
        addData.setTotalPayment(repairListRequest.getTotalPayment());
        addData.setInsuranceApplication(repairListRequest.getInsuranceApplication());
        addData.setRefundPrice(repairListRequest.getRefundPrice());
        addData.setAttackerName(repairListRequest.getAttackerName());
        addData.setPhoneNumber(repairListRequest.getPhoneNumber());
        addData.setEtcMemo(repairListRequest.getEtcMemo());

        repairListRepository.save(addData);
    }

    public List<RepairListItem> getRepairLists(){
        List<RepairList> originList = repairListRepository.findAll();

        List<RepairListItem> result = new LinkedList<>();

        for (RepairList repairList : originList){
            RepairListItem addItem = new RepairListItem();
            addItem.setId(repairList.getId());
            addItem.setAccidentDate(repairList.getAccidentDate());
            addItem.setAccidentPlace(repairList.getAccidentPlace());
            addItem.setAccidentContent(repairList.getAccidentContent().getAccidentContentEnum());
            addItem.setMistake(repairList.getMistake());
            addItem.setRepairShop(repairList.getRepairShop());
            addItem.setMaintenanceDetails(repairList.getMaintenanceDetails());
            addItem.setTotalPayment(repairList.getTotalPayment());
            addItem.setInsuranceApplication(repairList.getInsuranceApplication());
            addItem.setRefundPrice(repairList.getRefundPrice());
            addItem.setAttackerName(repairList.getAttackerName());
            addItem.setPhoneNumber(repairList.getPhoneNumber());
            addItem.setEtcMemo(repairList.getEtcMemo());

            result.add(addItem);
        }
        return result;
    }

    public RepairListResponse getRepairList(long id){
        RepairList originData = repairListRepository.findById(id).orElseThrow();

        RepairListResponse response = new RepairListResponse();
        response.setId(originData.getId());
        response.setAccidentDate(originData.getAccidentDate());
        response.setAccidentPlace(originData.getAccidentPlace());
        response.setAccidentContentName(originData.getAccidentContent().getAccidentContentEnum());
        response.setMistake(originData.getMistake()? "쌍방과실":"일반과실");
        response.setRepairShop(originData.getRepairShop());
        response.setMaintenanceDetails(originData.getMaintenanceDetails());
        response.setTotalPayment(originData.getTotalPayment());
        response.setInsuranceApplication(response.getInsuranceApplication());
        response.setRefundPrice(originData.getRefundPrice());
        response.setAttackerName(originData.getAttackerName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }
}
